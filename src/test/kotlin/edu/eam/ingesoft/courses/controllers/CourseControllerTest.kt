package edu.eam.ingesoft.courses.controllers

import CommonTests
import edu.eam.ingesoft.courses.config.Groups
import edu.eam.ingesoft.courses.config.Permissions
import edu.eam.ingesoft.courses.config.Routes
import edu.eam.ingesoft.courses.externalapi.model.requests.GetAcademicSpacesRequest
import edu.eam.ingesoft.courses.externalapi.model.responses.AcademicPeriodResponse
import edu.eam.ingesoft.courses.externalapi.model.responses.AcademicSpaceResponse
import edu.eam.ingesoft.courses.externalapi.model.responses.ProfessorResponse
import edu.eam.ingesoft.courses.externalapi.model.responses.StudentResponse
import edu.eam.ingesoft.courses.model.entities.Course
import edu.eam.ingesoft.courses.model.entities.CourseSchedule
import edu.eam.ingesoft.courses.model.entities.Time
import edu.eam.ingesoft.courses.model.enums.DayTimeEnum
import edu.eam.ingesoft.courses.model.requests.CourseEvaluationRequest
import org.hamcrest.Matchers
import org.junit.Assert
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import java.time.DayOfWeek

class CourseControllerTest : CommonTests() {

    @Test
    fun createCourseEvaluationTest() {

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.CREATE_COURSE_EVALUATION),
            groups = listOf(
                Groups.SYSTEM_ADMINISTRATOR,
                Groups.REGISTRATION_AND_CONTROL,
                Groups.PROGRAM_DIRECTOR
            )
        )

        fillCollection("courses", "/testData/courseControllerTest/createCourse.json")

        val request = MockMvcRequestBuilders.post("${Routes.PATH_COURSES}/${Routes.PATH_ADD_EVALUATION_COURSE}", "1e")
            .content(
                objectMapper.writeValueAsString(
                    CourseEvaluationRequest(
                        weight = 0.1,
                        name = "Homework 1"
                    )
                )
            )
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isOk)

        val courseEvaluationToAssert = courseEvaluationRepository.findAll().toList().find { it.name == "Homework 1" }

        Assert.assertNotNull(courseEvaluationToAssert)
        Assert.assertEquals("1e", courseEvaluationToAssert?.courseId)
        Assert.assertEquals("Homework 1", courseEvaluationToAssert?.name)
        Assert.assertEquals(0.1, courseEvaluationToAssert?.weight)
    }

    @Test
    fun createCourseEvaluationWhenTheCourseNotExistTest() {

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.CREATE_COURSE_EVALUATION),
            groups = listOf(
                Groups.SYSTEM_ADMINISTRATOR,
                Groups.REGISTRATION_AND_CONTROL,
                Groups.PROGRAM_DIRECTOR
            )
        )

        val request = MockMvcRequestBuilders.post("${Routes.PATH_COURSES}/${Routes.PATH_ADD_EVALUATION_COURSE}", "1e")
            .content(
                objectMapper.writeValueAsString(
                    CourseEvaluationRequest(
                        weight = 0.5,
                        name = "Homework 1"
                    )
                )
            )
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(404)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Course not found")))
    }

    @Test
    fun createCourseEvaluationWhenTheNameIsAlreadyInUseTest() {

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.CREATE_COURSE_EVALUATION),
            groups = listOf(
                Groups.SYSTEM_ADMINISTRATOR,
                Groups.REGISTRATION_AND_CONTROL,
                Groups.PROGRAM_DIRECTOR
            )
        )

        fillCollection("courses", "/testData/courseControllerTest/createCourse.json")
        fillCollection("course_evaluations", "/testData/courseControllerTest/courseEvaluations.json")

        val request = MockMvcRequestBuilders.post("${Routes.PATH_COURSES}/${Routes.PATH_ADD_EVALUATION_COURSE}", "1e")
            .content(
                objectMapper.writeValueAsString(
                    CourseEvaluationRequest(
                        weight = 0.5,
                        name = "Homework 1"
                    )
                )
            )
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(412)))
            .andExpect(
                MockMvcResultMatchers.jsonPath(
                    "$.message",
                    Matchers.`is`("Another evaluation is already using name of the current evaluation")
                )
            )
    }

    @Test
    fun createCourseEvaluationWhenTheWeightEnteredIsIncorrectTest() {

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.CREATE_COURSE_EVALUATION),
            groups = listOf(
                Groups.SYSTEM_ADMINISTRATOR,
                Groups.REGISTRATION_AND_CONTROL,
            )
        )

        fillCollection("courses", "/testData/courseControllerTest/createCourse.json")

        val request = MockMvcRequestBuilders.post("${Routes.PATH_COURSES}/${Routes.PATH_ADD_EVALUATION_COURSE}", "1e")
            .content(
                objectMapper.writeValueAsString(
                    CourseEvaluationRequest(
                        weight = -0.0,
                        name = "Homework 5"
                    )
                )
            )
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(412)))
            .andExpect(
                MockMvcResultMatchers.jsonPath(
                    "$.message",
                    Matchers.`is`("The weight entered must be greater than 0% and less than or equal to 100%")
                )
            )
    }

    @Test
    fun createCourseEvaluationWhenTheWeightExceedTheLimitTest() {

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.CREATE_COURSE_EVALUATION),
            groups = listOf(
                Groups.SYSTEM_ADMINISTRATOR,
                Groups.REGISTRATION_AND_CONTROL,
                Groups.PROGRAM_DIRECTOR
            )
        )

        fillCollection("courses", "/testData/courseControllerTest/createCourse.json")
        fillCollection("course_evaluations", "/testData/courseControllerTest/courseEvaluations.json")

        val request = MockMvcRequestBuilders.post("${Routes.PATH_COURSES}/${Routes.PATH_ADD_EVALUATION_COURSE}", "1e")
            .content(
                objectMapper.writeValueAsString(
                    CourseEvaluationRequest(
                        weight = 0.9,
                        name = "Homework 3"
                    )
                )
            )
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(412)))
            .andExpect(
                MockMvcResultMatchers.jsonPath(
                    "$.message",
                    Matchers.`is`("The weight entered is exceeds the 100% of the course evaluation")
                )
            )
    }

    @Test
    fun findCourseTest() {
        fillCollection("courses", "/testData/courseControllerTest/findCourse.json")

        val request = MockMvcRequestBuilders.get("${Routes.PATH_COURSES}/${Routes.PATH_FIND_COURSE_BY_ID}", "1234")
            .contentType(MediaType.APPLICATION_JSON)
        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(MockMvcResultMatchers.jsonPath("$.group", Matchers.`is`("A")))
            .andExpect(MockMvcResultMatchers.jsonPath("$.professor_id", Matchers.`is`("1234")))
            .andExpect(MockMvcResultMatchers.jsonPath("$.academic_space_id", Matchers.`is`("abcde")))
            .andExpect(MockMvcResultMatchers.jsonPath("$.day_time", Matchers.`is`("DAY")))
            .andExpect(MockMvcResultMatchers.jsonPath("$.academic_period", Matchers.`is`("20211")))
    }

    @Test
    fun findCourseNotFoundTest() {
        val request = MockMvcRequestBuilders.get("${Routes.PATH_COURSES}/${Routes.PATH_FIND_COURSE_BY_ID}", "1234")
            .contentType(MediaType.APPLICATION_JSON)
        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isNotFound)
    }

    @Test
    fun editCourseTest() {
        fillCollection("courses", "/testData/courseControllerTest/editCourse.json")

        val request = MockMvcRequestBuilders.put("${Routes.PATH_COURSES}/${Routes.PATH_FIND_COURSE_BY_ID}", "1234")
            .contentType(MediaType.APPLICATION_JSON)
            .content(
                objectMapper.writeValueAsString(
                    Course(
                        group = "B",
                        professorId = "5432",
                        dayTime = DayTimeEnum.NIGHT,
                        academicSpaceId = "XYZ",
                        academicPeriod = "20201"
                    )
                )
            )
            .content(
                objectMapper.writeValueAsString(
                    Course(
                        group = "B",
                        professorId = "5432",
                        dayTime = DayTimeEnum.NIGHT,
                        academicSpaceId = "XYZ",
                        academicPeriod = "20201"
                    )
                )
            )

            .content(
                objectMapper.writeValueAsString(
                    Course(
                        group = "B",
                        professorId = "5432",
                        dayTime = DayTimeEnum.NIGHT,
                        academicSpaceId = "XYZ",
                        academicPeriod = "20201"
                    )
                )
            )
        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(MockMvcResultMatchers.jsonPath("$.group", Matchers.`is`("B")))
            .andExpect(MockMvcResultMatchers.jsonPath("$.professor_id", Matchers.`is`("5432")))
            .andExpect(MockMvcResultMatchers.jsonPath("$.academic_space_id", Matchers.`is`("XYZ")))
            .andExpect(MockMvcResultMatchers.jsonPath("$.day_time", Matchers.`is`("NIGHT")))
            .andExpect(MockMvcResultMatchers.jsonPath("$.academic_period", Matchers.`is`("20201")))

        val courseToAssert = courseRepository.findById("1234").get()
        Assertions.assertEquals("B", courseToAssert.group)
        Assertions.assertEquals("5432", courseToAssert.professorId)
        Assertions.assertEquals("XYZ", courseToAssert.academicSpaceId)
        Assertions.assertEquals(DayTimeEnum.NIGHT, courseToAssert.dayTime)
        Assertions.assertEquals("20201", courseToAssert.academicPeriod)
    }

    @Test
    fun createScheduleTest() {

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.ADD_COURSE_SCHEDULE),
            groups = listOf(
                Groups.SYSTEM_ADMINISTRATOR,
                Groups.CLASS_TEACHER
            )
        )

        fillCollection("courses", "/testData/courseControllerTest/createScheduleTest.json")

        val schedule = CourseSchedule(DayOfWeek.FRIDAY, Time(10, 0), Time(12, 0))

        val request = MockMvcRequestBuilders
            .post("${Routes.PATH_COURSES}${Routes.SCHEDULE_PATH}", "1010")
            .content(objectMapper.writeValueAsString(schedule))
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isOk)

        val course = courseRepository.findById("1010").get()

        Assertions.assertEquals(DayOfWeek.FRIDAY, course.courseSchedule?.get(2)?.dayOfWeek)
        Assertions.assertEquals(10, course.courseSchedule?.get(2)?.startsAt?.hour)
        Assertions.assertEquals(12, course.courseSchedule?.get(2)?.endsAt?.hour)
    }

    @Test
    fun createScheduleWhenCourseDontExistTest() {

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.ADD_COURSE_SCHEDULE),
            groups = listOf(
                Groups.SYSTEM_ADMINISTRATOR,
                Groups.CLASS_TEACHER
            )
        )

        fillCollection("courses", "/testData/courseControllerTest/createScheduleTest.json")

        val schedule = CourseSchedule(DayOfWeek.FRIDAY, Time(10, 0), Time(12, 0))

        val request = MockMvcRequestBuilders
            .post("${Routes.PATH_COURSES}${Routes.SCHEDULE_PATH}", "2020")
            .content(objectMapper.writeValueAsString(schedule))
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isNotFound)
    }

    @Test
    fun createScheduleWhenScheduleCrossInDifferentAcademicPeriodTest() {

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.ADD_COURSE_SCHEDULE),
            groups = listOf(
                Groups.SYSTEM_ADMINISTRATOR,
                Groups.CLASS_TEACHER
            )
        )

        fillCollection("courses", "/testData/courseControllerTest/createScheduleTest.json")

        val schedule = CourseSchedule(DayOfWeek.FRIDAY, Time(14, 30), Time(16, 0))

        val request = MockMvcRequestBuilders
            .post("${Routes.PATH_COURSES}${Routes.SCHEDULE_PATH}", "4040")
            .content(objectMapper.writeValueAsString(schedule))
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isOk)

        val course = courseRepository.findById("4040").get()

        Assertions.assertEquals(DayOfWeek.FRIDAY, course.courseSchedule?.get(1)?.dayOfWeek)
        Assertions.assertEquals(14, course.courseSchedule?.get(1)?.startsAt?.hour)
        Assertions.assertEquals(16, course.courseSchedule?.get(1)?.endsAt?.hour)
    }

    @Test
    fun createScheduleWhenScheduleCrossButWithDifferentProffesorsTest() {

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.ADD_COURSE_SCHEDULE),
            groups = listOf(
                Groups.SYSTEM_ADMINISTRATOR,
                Groups.CLASS_TEACHER
            )
        )

        fillCollection("courses", "/testData/courseControllerTest/createScheduleTest.json")

        val schedule = CourseSchedule(DayOfWeek.FRIDAY, Time(14, 30), Time(16, 0))

        val request = MockMvcRequestBuilders
            .post("${Routes.PATH_COURSES}${Routes.SCHEDULE_PATH}", "5050")
            .content(objectMapper.writeValueAsString(schedule))
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isOk)

        val course = courseRepository.findById("5050").get()

        Assertions.assertEquals(DayOfWeek.FRIDAY, course.courseSchedule?.get(1)?.dayOfWeek)
        Assertions.assertEquals(14, course.courseSchedule?.get(1)?.startsAt?.hour)
        Assertions.assertEquals(16, course.courseSchedule?.get(1)?.endsAt?.hour)
    }

    @Test
    fun createScheduleWhenScheduleCrossButWithDifferentDayTest() {

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.ADD_COURSE_SCHEDULE),
            groups = listOf(
                Groups.SYSTEM_ADMINISTRATOR,
                Groups.CLASS_TEACHER
            )
        )

        fillCollection("courses", "/testData/courseControllerTest/createScheduleTest.json")

        val schedule = CourseSchedule(DayOfWeek.MONDAY, Time(14, 30), Time(16, 0))

        val request = MockMvcRequestBuilders
            .post("${Routes.PATH_COURSES}${Routes.SCHEDULE_PATH}", "1010")
            .content(objectMapper.writeValueAsString(schedule))
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isOk)

        val course = courseRepository.findById("1010").get()

        Assertions.assertEquals(DayOfWeek.MONDAY, course.courseSchedule?.get(2)?.dayOfWeek)
        Assertions.assertEquals(14, course.courseSchedule?.get(2)?.startsAt?.hour)
        Assertions.assertEquals(16, course.courseSchedule?.get(2)?.endsAt?.hour)
    }

    @Test
    fun createScheduleWhenScheduleCrossInTheSameCourseTest() {

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.ADD_COURSE_SCHEDULE),
            groups = listOf(
                Groups.SYSTEM_ADMINISTRATOR,
                Groups.CLASS_TEACHER
            )
        )

        fillCollection("courses", "/testData/courseControllerTest/createScheduleTest.json")

        val schedule = CourseSchedule(DayOfWeek.FRIDAY, Time(14, 30), Time(16, 0))

        val request = MockMvcRequestBuilders
            .post("${Routes.PATH_COURSES}${Routes.SCHEDULE_PATH}", "1010")
            .content(objectMapper.writeValueAsString(schedule))
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(
                MockMvcResultMatchers.jsonPath(
                    "$.message",
                    Matchers.`is`("Schedule already programmed in that time period")
                )
            )

        val course = courseRepository.findById("1010").get()

        Assertions.assertEquals(2, course.courseSchedule?.size)
    }

    @Test
    fun createScheduleWhenScheduleCrossInDifferentsCoursesTest() {

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.ADD_COURSE_SCHEDULE),
            groups = listOf(
                Groups.SYSTEM_ADMINISTRATOR,
                Groups.CLASS_TEACHER
            )
        )

        fillCollection("courses", "/testData/courseControllerTest/createScheduleTest.json")

        val schedule = CourseSchedule(DayOfWeek.FRIDAY, Time(14, 30), Time(16, 0))

        val request = MockMvcRequestBuilders
            .post("${Routes.PATH_COURSES}${Routes.SCHEDULE_PATH}", "3030")
            .content(objectMapper.writeValueAsString(schedule))
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(
                MockMvcResultMatchers.jsonPath(
                    "$.message",
                    Matchers.`is`("Schedule already programmed in that time period")
                )
            )

        val course = courseRepository.findById("3030").get()

        Assertions.assertEquals(1, course.courseSchedule?.size)
    }

    @Test
    fun createScheduleWhenScheduleCrossRightTest() {

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.ADD_COURSE_SCHEDULE),
            groups = listOf(
                Groups.SYSTEM_ADMINISTRATOR,
                Groups.CLASS_TEACHER
            )
        )

        fillCollection("courses", "/testData/courseControllerTest/createScheduleTest.json")

        val schedule = CourseSchedule(DayOfWeek.FRIDAY, Time(10, 0), Time(12, 30))

        val request = MockMvcRequestBuilders
            .post("${Routes.PATH_COURSES}${Routes.SCHEDULE_PATH}", "1010")
            .content(objectMapper.writeValueAsString(schedule))
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(
                MockMvcResultMatchers.jsonPath(
                    "$.message",
                    Matchers.`is`("Schedule already programmed in that time period")
                )
            )

        val course = courseRepository.findById("1010").get()

        Assertions.assertEquals(2, course.courseSchedule?.size)
    }

    @Test
    fun createScheduleWhenScheduleCrossLeftTest() {

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.ADD_COURSE_SCHEDULE),
            groups = listOf(
                Groups.SYSTEM_ADMINISTRATOR,
                Groups.CLASS_TEACHER
            )
        )

        fillCollection("courses", "/testData/courseControllerTest/createScheduleTest.json")

        val schedule = CourseSchedule(DayOfWeek.FRIDAY, Time(15, 50), Time(18, 0))

        val request = MockMvcRequestBuilders
            .post("${Routes.PATH_COURSES}${Routes.SCHEDULE_PATH}", "1010")
            .content(objectMapper.writeValueAsString(schedule))
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(
                MockMvcResultMatchers.jsonPath(
                    "$.message",
                    Matchers.`is`("Schedule already programmed in that time period")
                )
            )

        val course = courseRepository.findById("1010").get()

        Assertions.assertEquals(2, course.courseSchedule?.size)
    }

    @Test
    fun createScheduleWhenScheduleWithInvalidTimeLapsTest() {

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.ADD_COURSE_SCHEDULE),
            groups = listOf(
                Groups.SYSTEM_ADMINISTRATOR,
                Groups.CLASS_TEACHER
            )
        )

        fillCollection("courses", "/testData/courseControllerTest/createScheduleTest.json")

        val schedule = CourseSchedule(DayOfWeek.FRIDAY, Time(18, 0), Time(15, 0))

        val request = MockMvcRequestBuilders
            .post("${Routes.PATH_COURSES}${Routes.SCHEDULE_PATH}", "1010")
            .content(objectMapper.writeValueAsString(schedule))
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Invalid Schedule's Duration")))

        val course = courseRepository.findById("1010").get()

        Assertions.assertEquals(2, course.courseSchedule?.size)
    }

    @Test
    fun findQualificationsByStudentTest() {

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.FIND_QUALIFICATIONS_BY_STUDENT),
            groups = listOf(
                Groups.SYSTEM_ADMINISTRATOR,
                Groups.CLASS_TEACHER
            )
        )

        fillCollection("courses", "/testData/courseControllerTest/createCourse.json")
        fillCollection("course_evaluations", "/testData/courseControllerTest/courseEvaluations.json")
        fillCollection("course_students", "/testData/courseControllerTest/findQualificationStudentWithNotes.json")

        Mockito.`when`(studentMsClient.getStudentByCode("240220191020")).thenReturn(StudentResponse("240220191020"))

        Mockito.`when`(academicMsClient.getAcademicSpacesByIds(GetAcademicSpacesRequest(listOf(100, 101))))
            .thenReturn(
                listOf(AcademicSpaceResponse(100L, "Algebra"), AcademicSpaceResponse(101L, "Web"))
            )

        val request = MockMvcRequestBuilders.get(
            "${Routes.PATH_COURSES}/${Routes.PATH_FIND_QUALIFICATIONS_BY_STUDENT}",
            "240220191020"
        )
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize<Int>(2)))
            .andExpect(MockMvcResultMatchers.jsonPath("$[0].course.name", Matchers.`is`("Algebra")))
            .andExpect(MockMvcResultMatchers.jsonPath("$[1].course.name", Matchers.`is`("Web")))
            .andExpect(MockMvcResultMatchers.jsonPath("$[0].qualification", Matchers.hasSize<Int>(2)))
            .andExpect(MockMvcResultMatchers.jsonPath("$[0].qualification", Matchers.hasSize<Int>(2)))
            .andExpect(MockMvcResultMatchers.jsonPath("$[0].final_note", Matchers.`is`(2.05)))
            .andExpect(MockMvcResultMatchers.jsonPath("$[1].final_note", Matchers.`is`(2.2)))
    }

    @Test
    fun findQualificationsByStudentWhenTheStudentNotExistTest() {

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.FIND_QUALIFICATIONS_BY_STUDENT),
            groups = listOf(
                Groups.SYSTEM_ADMINISTRATOR,
                Groups.CLASS_TEACHER
            )
        )

        fillCollection("courses", "/testData/courseControllerTest/createCourse.json")
        fillCollection("course_evaluations", "/testData/courseControllerTest/courseEvaluations.json")
        fillCollection("course_students", "/testData/courseControllerTest/findQualificationStudentWithNotes.json")

        Mockito.`when`(studentMsClient.getStudentByCode("240220191020")).thenThrow(createFeignNotFoundException())

        Mockito.`when`(academicMsClient.getAcademicSpacesByIds(GetAcademicSpacesRequest(listOf(100, 101))))
            .thenReturn(
                listOf(AcademicSpaceResponse(100L, "Algebra"), AcademicSpaceResponse(101L, "Web"))
            )

        val request = MockMvcRequestBuilders.get(
            "${Routes.PATH_COURSES}/${Routes.PATH_FIND_QUALIFICATIONS_BY_STUDENT}",
            "240220191020"
        )
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(404)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Student Not Found")))
    }

    @Test
    fun findQualificationsByStudentWhenTheStudentIsNotAssignedACourseTest() {

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.FIND_QUALIFICATIONS_BY_STUDENT),
            groups = listOf(
                Groups.SYSTEM_ADMINISTRATOR,
                Groups.CLASS_TEACHER
            )
        )

        fillCollection("courses", "/testData/courseControllerTest/createCourse.json")
        fillCollection("course_evaluations", "/testData/courseControllerTest/courseEvaluations.json")
        fillCollection("course_students", "/testData/courseControllerTest/findQualificationStudentWithNotes.json")

        Mockito.`when`(studentMsClient.getStudentByCode("1")).thenReturn(StudentResponse("1"))

        Mockito.`when`(academicMsClient.getAcademicSpacesByIds(GetAcademicSpacesRequest(listOf(100, 101))))
            .thenReturn(
                listOf(AcademicSpaceResponse(100L, "Algebra"), AcademicSpaceResponse(101L, "Web"))
            )

        val request = MockMvcRequestBuilders.get(
            "${Routes.PATH_COURSES}/${Routes.PATH_FIND_QUALIFICATIONS_BY_STUDENT}",
            "240220191021"
        )
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        mockMvc.perform(request)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(404)))
            .andExpect(
                MockMvcResultMatchers.jsonPath(
                    "$.message",
                    Matchers.`is`("Student has not been assigned a course")
                )
            )
    }

    @Test
    fun findQualificationsByStudentWhenWithoutNotesTest() {

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.FIND_QUALIFICATIONS_BY_STUDENT),
            groups = listOf(
                Groups.SYSTEM_ADMINISTRATOR,
                Groups.CLASS_TEACHER
            )
        )

        fillCollection("courses", "/testData/courseControllerTest/createCourse.json")
        fillCollection("course_evaluations", "/testData/courseControllerTest/courseEvaluations.json")
        fillCollection("course_students", "/testData/courseControllerTest/findQualificationStudentWithoutNotes.json")

        Mockito.`when`(studentMsClient.getStudentByCode("240220191020")).thenReturn(StudentResponse("240220191020"))

        Mockito.`when`(academicMsClient.getAcademicSpacesByIds(GetAcademicSpacesRequest(listOf(100, 101))))
            .thenReturn(
                listOf(AcademicSpaceResponse(100L, "Algebra"), AcademicSpaceResponse(101L, "Web"))
            )

        val request = MockMvcRequestBuilders.get(
            "${Routes.PATH_COURSES}/${Routes.PATH_FIND_QUALIFICATIONS_BY_STUDENT}",
            "240220191020"
        )
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize<Int>(2)))
            .andExpect(MockMvcResultMatchers.jsonPath("$[0].course.name", Matchers.`is`("Algebra")))
            .andExpect(MockMvcResultMatchers.jsonPath("$[1].course.name", Matchers.`is`("Web")))
            .andExpect(MockMvcResultMatchers.jsonPath("$[0].qualification", Matchers.hasSize<Int>(0)))
            .andExpect(MockMvcResultMatchers.jsonPath("$[0].qualification", Matchers.hasSize<Int>(0)))
            .andExpect(MockMvcResultMatchers.jsonPath("$[0].final_note", Matchers.`is`(0.0)))
            .andExpect(MockMvcResultMatchers.jsonPath("$[1].final_note", Matchers.`is`(0.0)))
    }

    @Test
    fun createCourseTest() {

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.CREATE_COURSE),
            groups = listOf(
                Groups.SYSTEM_ADMINISTRATOR,
                Groups.REGISTRATION_AND_CONTROL,
                Groups.PROGRAM_DIRECTOR
            )
        )

        val jsonBody =
            """{
            "group":"A",
            "professor_id": "10",
            "academic_space_id":"2",
            "dayTime":"DAY"
            }"""
        Mockito.`when`(academicMsClient.getAcademicPeriod()).thenReturn(AcademicPeriodResponse("20211"))
        Mockito.`when`(academicMsClient.getAcademicSpace("2")).thenReturn(AcademicSpaceResponse(2L, "Web"))
        Mockito.`when`(professorMsClient.getProfessor("10")).thenReturn(ProfessorResponse("10", "10"))
        val request = MockMvcRequestBuilders.post("${Routes.PATH_COURSES}")
            .content(jsonBody).contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isOk)

        val course = courseRepository.findAll().first()

        Assertions.assertEquals("A", course.group)
        Assertions.assertEquals("10", course.professorId)
        Assertions.assertEquals("2", course.academicSpaceId)
    }

    @Test
    fun createCourseTestWithNoPermmision() {

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.ADD_COURSE_SCHEDULE),
            groups = listOf(
                Groups.SYSTEM_ADMINISTRATOR,
                Groups.REGISTRATION_AND_CONTROL,
                Groups.PROGRAM_DIRECTOR
            )
        )

        val jsonBody =
            """{
            "group":"A",
            "professor_id": "10",
            "academic_space_id":"2",
            "dayTime":"DAY"
            }"""
        Mockito.`when`(academicMsClient.getAcademicPeriod()).thenReturn(AcademicPeriodResponse("20211"))
        Mockito.`when`(academicMsClient.getAcademicSpace("2")).thenReturn(AcademicSpaceResponse(2L, "Web"))
        Mockito.`when`(professorMsClient.getProfessor("10")).thenReturn(ProfessorResponse("10", "10"))
        val request = MockMvcRequestBuilders.post("${Routes.PATH_COURSES}")
            .content(jsonBody).contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request).andExpect(MockMvcResultMatchers.status().isForbidden)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(403)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("this user has no access")))
    }

    @Test
    fun createCourseTestWithNoHeader() {

        val jsonBody =
            """{
            "group":"A",
            "professor_id": "10",
            "academic_space_id":"2",
            "dayTime":"DAY"
            }"""
        Mockito.`when`(academicMsClient.getAcademicPeriod()).thenReturn(AcademicPeriodResponse("20211"))
        Mockito.`when`(academicMsClient.getAcademicSpace("2")).thenReturn(AcademicSpaceResponse(2L, "Web"))
        Mockito.`when`(professorMsClient.getProfessor("10")).thenReturn(ProfessorResponse("10", "10"))
        val request = MockMvcRequestBuilders.post("${Routes.PATH_COURSES}")
            .content(jsonBody).contentType(MediaType.APPLICATION_JSON)

        val response = mockMvc.perform(request).andExpect(MockMvcResultMatchers.status().isForbidden)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(403)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("token required")))
    }

    @Test
    fun createCourseTestWithNoTokenValid() {

        val header = mockSecurity(

            tokenValue = "Token",

            permissions = listOf(Permissions.CREATE_COURSE),

            groups = listOf(
                Groups.SYSTEM_ADMINISTRATOR,
                Groups.REGISTRATION_AND_CONTROL,
                Groups.PROGRAM_DIRECTOR
            )

        )

        val jsonBody =
            """{
            "group":"A",
            "professor_id": "10",
            "academic_space_id":"2",
            "dayTime":"DAY"
            }"""
        Mockito.`when`(academicMsClient.getAcademicPeriod()).thenReturn(AcademicPeriodResponse("20211"))
        Mockito.`when`(academicMsClient.getAcademicSpace("2")).thenReturn(AcademicSpaceResponse(2L, "Web"))
        Mockito.`when`(professorMsClient.getProfessor("10")).thenReturn(ProfessorResponse("10", "10"))
        val request = MockMvcRequestBuilders.post("${Routes.PATH_COURSES}")
            .content(jsonBody).contentType(MediaType.APPLICATION_JSON)
            .header(header.name, "WrongToken")
        val response = mockMvc.perform(request).andExpect(MockMvcResultMatchers.status().isForbidden)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(403)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("invalid token")))
    }

    @Test
    fun createCourseRepeatTest() {
        fillCollection("courses", "/testData/courseControllerTest/createCourseRepeat.json")
        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.CREATE_COURSE),
            groups = listOf(
                Groups.SYSTEM_ADMINISTRATOR,
                Groups.REGISTRATION_AND_CONTROL,
                Groups.PROGRAM_DIRECTOR
            )
        )

        val jsonBody =
            """{
            "group":"A",
            "professor_id": "10",
            "academic_space_id":"2",
            "dayTime":"DAY"
            }"""
        Mockito.`when`(academicMsClient.getAcademicPeriod()).thenReturn(AcademicPeriodResponse("20211"))
        Mockito.`when`(academicMsClient.getAcademicSpace("2")).thenReturn(AcademicSpaceResponse(2L, "Web"))
        Mockito.`when`(professorMsClient.getProfessor("10")).thenReturn(ProfessorResponse("10", "10"))

        val request = MockMvcRequestBuilders.post("${Routes.PATH_COURSES}")
            .content(jsonBody).contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)
        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(412)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Course is already registered.")))
    }

    @Test
    fun createCourseProfessorNotFoundTest() {

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.CREATE_COURSE),
            groups = listOf(
                Groups.SYSTEM_ADMINISTRATOR,
                Groups.REGISTRATION_AND_CONTROL,
                Groups.PROGRAM_DIRECTOR
            )
        )

        val jsonBody =
            """{
            "group":"A",
            "professor_id": "100",
            "academic_space_id":"2",
            "dayTime":"DAY"
            }"""

        Mockito.`when`(academicMsClient.getAcademicPeriod()).thenReturn(AcademicPeriodResponse("20211"))
        Mockito.`when`(academicMsClient.getAcademicSpace("2")).thenReturn(AcademicSpaceResponse(2L, "Web"))
        Mockito.`when`(professorMsClient.getProfessor("100")).thenThrow(createFeignNotFoundException())
        val request = MockMvcRequestBuilders.post("${Routes.PATH_COURSES}")

            .content(jsonBody).contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isNotFound)

            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(404)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Professor not found.")))
    }

    @Test
    fun createCourseAcademicSpaceNotFoundTest() {

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.CREATE_COURSE),
            groups = listOf(
                Groups.SYSTEM_ADMINISTRATOR,
                Groups.REGISTRATION_AND_CONTROL,
                Groups.PROGRAM_DIRECTOR
            )
        )

        val jsonBody =
            """{
            "group":"A",
            "professor_id": "10",
            "academic_space_id":"200",
            "dayTime":"DAY"
            }"""
        Mockito.`when`(academicMsClient.getAcademicPeriod()).thenReturn(AcademicPeriodResponse("20211"))
        Mockito.`when`(professorMsClient.getProfessor("10")).thenReturn(ProfessorResponse("10", "10"))
        Mockito.`when`(academicMsClient.getAcademicSpace("200")).thenThrow(createFeignNotFoundException())

        val request = MockMvcRequestBuilders.post("${Routes.PATH_COURSES}")

            .content(jsonBody).contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isNotFound)

            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(404)))

            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Academic space doesn't exist.")))
    }

    @Test
    fun deliverStudentGrade() {

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.DELIVER_STUDENT_GRADE),
            groups = listOf(
                Groups.CLASS_TEACHER,
            )
        )

        fillCollection("courses", "/testData/courseControllerTest/deliverGradeStudentCourse.json")
        fillCollection("course_students", "/testData/courseControllerTest/deliverGradeStudent.json")

        val request =
            MockMvcRequestBuilders.patch("${Routes.PATH_COURSES}/${Routes.DELIVER_GRADE_PATH}", "911", "123", "1")
                .contentType(MediaType.APPLICATION_JSON)
                .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isOk)

        var courseStudentToAssert = courseStudentRepository.findById("1").get()

        Assertions.assertEquals(true, courseStudentToAssert.courseStudentQualification?.get(0)?.delivered)
    }

    @Test
    fun deliverStudentGradeIsAlreadyDeliver() {

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.DELIVER_STUDENT_GRADE),
            groups = listOf(
                Groups.CLASS_TEACHER,
            )
        )

        fillCollection("courses", "/testData/courseControllerTest/deliverStudentGradeIsAlreadyDeliverCoureses.json")
        fillCollection(
            "course_students",
            "/testData/courseControllerTest/deliverStudentGradeIsAlreadyDeliverStudentCourse.json"
        )

        val request =
            MockMvcRequestBuilders.patch("${Routes.PATH_COURSES}/${Routes.DELIVER_GRADE_PATH}", "911", "123", "1")
                .contentType(MediaType.APPLICATION_JSON)
                .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(412)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("This grade is alredy delivered")))
    }

    @Test
    fun deliverStudentGradeNotFoundCourse() {

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.DELIVER_STUDENT_GRADE),
            groups = listOf(
                Groups.CLASS_TEACHER,
            )
        )

        fillCollection("courses", "/testData/courseControllerTest/deliverStudentGradeNotFoundCourseCourse.json")
        fillCollection(
            "course_students",
            "/testData/courseControllerTest/deliverStudentGradeNotFoundCourseCourseStudent.json"
        )

        val request =
            MockMvcRequestBuilders.patch("${Routes.PATH_COURSES}/${Routes.DELIVER_GRADE_PATH}", "912", "123", "1")
                .contentType(MediaType.APPLICATION_JSON)
                .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(404)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("The course does not exist")))
    }

    @Test
    fun deliverStudentGradeNotFoundEvaluation() {

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.DELIVER_STUDENT_GRADE),
            groups = listOf(
                Groups.CLASS_TEACHER,
            )
        )

        fillCollection("courses", "/testData/courseControllerTest/deliverStudentGradeNotFoundEvaluationCourse.json")

        fillCollection(
            "course_students",
            "/testData/courseControllerTest/deliverStudentGradeNotFoundEvaluationCourseStudent.json"
        )

        val request =
            MockMvcRequestBuilders.patch("${Routes.PATH_COURSES}/${Routes.DELIVER_GRADE_PATH}", "911", "123", "22")
                .contentType(MediaType.APPLICATION_JSON)
                .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(404)))
            .andExpect(
                MockMvcResultMatchers.jsonPath(
                    "$.message",
                    Matchers.`is`("The evaluation does not exist on this curse student")
                )
            )
    }

    @Test
    fun findQualificationsByCourseTest() {

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.FIND_QUALIFICATION_BY_COURSE),
            groups = listOf(
                Groups.CLASS_TEACHER,
                Groups.REGISTRATION_AND_CONTROL,
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR
            )
        )

        fillCollection("courses", "/testData/courseControllerTest/createCourses.json")
        fillCollection("course_evaluations", "/testData/courseControllerTest/courseEvaluetins.json")
        fillCollection("course_students", "/testData/courseControllerTest/qualificationStudent.json")

        val request =
            MockMvcRequestBuilders.get("${Routes.PATH_COURSES}/${Routes.PATH_FIND_QUALIFICATION_BY_COURSE}", "1qw")
                .contentType(MediaType.APPLICATION_JSON)
                .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(MockMvcResultMatchers.jsonPath("content", Matchers.hasSize<Int>(2)))
            .andExpect(MockMvcResultMatchers.jsonPath("content[0].final_note", Matchers.`is`(0.55)))
            .andExpect(MockMvcResultMatchers.jsonPath("content[1].final_note", Matchers.`is`(0.65)))
    }

    @Test
    fun findQualificationsByCourseNotExistTest() {

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.FIND_QUALIFICATION_BY_COURSE),
            groups = listOf(
                Groups.CLASS_TEACHER,
                Groups.REGISTRATION_AND_CONTROL,
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR
            )
        )

        fillCollection("courses", "/testData/courseControllerTest/createCourses.json")
        fillCollection("course_evaluations", "/testData/courseControllerTest/courseEvaluetins.json")
        fillCollection("course_students", "/testData/courseControllerTest/qualificationStudent.json")

        val request =
            MockMvcRequestBuilders.get("${Routes.PATH_COURSES}/${Routes.PATH_FIND_QUALIFICATION_BY_COURSE}", "1lp")
                .contentType(MediaType.APPLICATION_JSON)
                .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(404)))
    }
}
