package edu.eam.ingesoft.courses.controllers

import CommonTests
import edu.eam.ingesoft.courses.config.Routes
import edu.eam.ingesoft.courses.externalapi.model.responses.StudentResponse
import org.hamcrest.Matchers
import org.junit.Assert
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

class StudentControllerTest : CommonTests() {

    @Test
    fun createCourseStudentTest() {

        fillCollection("courses", "/testData/studentControllerTest/createCourse.json")

        val studentId: String = "123"
        val courseId: String = "606f8380b5746f0a8dce7f91"

        val jsonBody =
            """{
                "student_id": "$studentId"
            }"""

        val studentResponse: StudentResponse = StudentResponse("123")

        Mockito.`when`(studentMsClient.getStudentByCode("123")).thenReturn(studentResponse)

        val request = MockMvcRequestBuilders
            .post("${Routes.PATH_COURSES}/${Routes.CREATE_STUDENT_COURSE_PATH}", courseId)
            .content(jsonBody)
            .contentType(MediaType.APPLICATION_JSON_VALUE)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isOk)

        val courseStudents = courseStudentRepository.findAll().toList()

        Assert.assertEquals("123", courseStudents[0].studentId)
        Assert.assertEquals("606f8380b5746f0a8dce7f91", courseStudents[0].courseId)
    }

    @Test
    fun createCourseStudentWithoutCourse() {
        val studentId: String = "123"
        val courseId: String = "606f8380b5746f0a8dce7f91"

        val jsonBody =
            """{
                "student_id": "$studentId"
            }"""

        val request = MockMvcRequestBuilders
            .post("${Routes.PATH_COURSES}/${Routes.CREATE_STUDENT_COURSE_PATH}", courseId)
            .content(jsonBody)
            .contentType(MediaType.APPLICATION_JSON_VALUE)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(404)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Course Not Found")))
    }

    @Test
    fun createCourseStudentAlreadyInCourseTest() {

        fillCollection("courses", "/testData/studentControllerTest/createCourse.json")

        fillCollection("course_students", "/testData/studentControllerTest/createCourseStudentAlreadyInCourse.json")

        val studentId: String = "123"
        val courseId: String = "606f8380b5746f0a8dce7f91"

        val jsonBody =
            """{
                "student_id": "$studentId"
            }"""

        val request = MockMvcRequestBuilders
            .post("${Routes.PATH_COURSES}/${Routes.CREATE_STUDENT_COURSE_PATH}", courseId)
            .content(jsonBody)
            .contentType(MediaType.APPLICATION_JSON_VALUE)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(412)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Student Is Already In The Course")))
    }

    @Test
    fun createCourseStudentNotFoundStudentTest() {

        fillCollection("courses", "/testData/studentControllerTest/createCourse.json")

        val studentId: String = "321"
        val courseId: String = "606f8380b5746f0a8dce7f91"

        val jsonBody =
            """{
                "student_id": "$studentId"
            }"""

        Mockito.`when`(studentMsClient.getStudentByCode("321")).thenThrow(createFeignNotFoundException())

        val request = MockMvcRequestBuilders
            .post("${Routes.PATH_COURSES}/${Routes.CREATE_STUDENT_COURSE_PATH}", courseId)
            .content(jsonBody)
            .contentType(MediaType.APPLICATION_JSON_VALUE)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(404)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Student Not Found")))
    }
}
