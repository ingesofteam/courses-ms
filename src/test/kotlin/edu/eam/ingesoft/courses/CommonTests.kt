import com.fasterxml.jackson.databind.ObjectMapper
import edu.eam.ingesoft.courses.externalapi.client.StudentsMsClient
import edu.eam.ingesoft.courses.externalapi.clients.AcademicMsClient
import edu.eam.ingesoft.courses.externalapi.clients.ProfessorMsClient
import edu.eam.ingesoft.courses.model.entities.Course
import edu.eam.ingesoft.courses.model.entities.CourseEvaluation
import edu.eam.ingesoft.courses.model.entities.CourseProfessorLog
import edu.eam.ingesoft.courses.model.entities.CourseStudent
import edu.eam.ingesoft.courses.repositories.CourseEvaluationRepository
import edu.eam.ingesoft.courses.repositories.CourseRepository
import edu.eam.ingesoft.courses.repositories.CourseStudentRepository
import edu.eam.ingesoft.courses.security.authclient.SecurityClient
import edu.eam.ingesoft.courses.security.authclient.model.Header
import edu.eam.ingesoft.courses.security.authclient.model.SecurityPayload
import edu.eam.ingesoft.courses.security.authclient.model.Token
import feign.FeignException
import feign.Request
import org.bson.Document
import org.junit.jupiter.api.BeforeEach
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.web.servlet.MockMvc

@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
abstract class CommonTests {

    @Autowired
    protected lateinit var objectMapper: ObjectMapper

    @Autowired
    protected lateinit var courseRepository: CourseRepository

    @Autowired
    protected lateinit var courseStudentRepository: CourseStudentRepository

    @Autowired
    protected lateinit var mockMvc: MockMvc

    @Autowired
    protected lateinit var mongoTemplate: MongoTemplate

    @Autowired
    protected lateinit var courseEvaluationRepository: CourseEvaluationRepository

    @MockBean
    protected lateinit var academicMsClient: AcademicMsClient

    @MockBean
    protected lateinit var studentMsClient: StudentsMsClient

    @MockBean
    protected lateinit var professorMsClient: ProfessorMsClient

    @MockBean
    protected lateinit var securityClient: SecurityClient

    @BeforeEach
    fun clearDB() {
        mongoTemplate.dropCollection(Course::class.java)
        mongoTemplate.dropCollection(CourseStudent::class.java)
        mongoTemplate.dropCollection(CourseEvaluation::class.java)
        mongoTemplate.dropCollection(CourseProfessorLog::class.java)
    }

    protected fun fillCollection(collectionName: String, fileName: String) {
        val text = this::class.java.getResource(fileName).readText(Charsets.UTF_8)

        val collection = mongoTemplate.createCollection(collectionName)
        val objects = objectMapper.readTree(text)

        val docs = objects.map { it -> Document.parse(it.toString()) }

        collection.insertMany(docs)
    }

    protected fun createFeignNotFoundException(
        body: String = "",
        method: Request.HttpMethod = Request.HttpMethod.POST
    ) =
        FeignException.NotFound(
            "",
            Request.create(method, "null", emptyMap(), "".toByteArray(), null, null),
            body.toByteArray()
        )
    protected fun mockSecurity(
        tokenValue: String = "SecurityToken",
        permissions: List<String> = listOf(),
        groups: List<String> = listOf()
    ): Header {
        val token = Token(tokenValue)
        val payload = SecurityPayload(
            "username",
            permissions,
            groups
        )
        Mockito.`when`(securityClient.validateToken(token)).thenReturn(payload)
        return Header(name = "Authorization", bearer = "Bearer $tokenValue")
    }
}
