package edu.eam.ingesoft.courses.config

object Routes {
    const val PATH = "/path"
    const val PATH_COURSES = "/courses"
    const val PATH_FIND_COURSE_BY_ID = "/{courseId}"
    const val STUDENT_PATH = "/student"
    const val PROFESSORS_PATH = "/professors"
    const val PATH_ADD_EVALUATION_COURSE = "/{courseId}/evaluations/"
    const val PATH_FIND_QUALIFICATIONS_BY_STUDENT = "/students/{studentCode}/qualifications/"
    const val PATH_EDIT_COURSE_BY_ID = "/{courseId}"
    const val SCHEDULE_PATH = "/{courseId}/schedules"
    const val ADD_FAILURE_BY_COURSE_ID_AND_COURSE_STUDENT_ID = "/{courseId}/student/{studentId}/failure"
    const val ADD_GRADE_BY_COURSE_ID_AND_COURSE_STUDENT_ID = "/{courseId}/student/{studentId}/grade"
    const val CREATE_STUDENT_COURSE_PATH = "/{courseId}/students"
    const val DELIVER_GRADE_PATH = "{courseId}/students/{studentId}/evaluations/{evaluationId}/deliver"
    const val EDIT_PROFESSOR_LOG = "/{courseId}/log/{id}"
    const val DELETE_PROFESSOR_LOG = "/{courseId}/log/{id}"
    const val PATH_FIND_QUALIFICATION_BY_COURSE = "/{courseId}/qualification"
    const val PATH_CREATE_PROFESSOR_LOG = "/{courseId}/professorLog/{id}"
    const val PATH_EDIT_COURSE_STUDENT_CALIFICATION = "/{courseId}/student/{studentId}/grade/edit"
}
