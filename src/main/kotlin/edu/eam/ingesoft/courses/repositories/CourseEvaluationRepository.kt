package edu.eam.ingesoft.courses.repositories

import edu.eam.ingesoft.courses.model.entities.CourseEvaluation
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
interface CourseEvaluationRepository : MongoRepository<CourseEvaluation, String> {
    fun existsByName(name: String): Boolean

    fun findByCourseId(courseId: String): List<CourseEvaluation>
}
