package edu.eam.ingesoft.courses.repositories

import edu.eam.ingesoft.courses.model.entities.Course
import edu.eam.ingesoft.courses.model.enums.DayTimeEnum
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.data.mongodb.repository.Query
import org.springframework.stereotype.Repository
import java.time.DayOfWeek

@Repository
interface CourseRepository : MongoRepository<Course, String> {

    @Query(
        "{\$and: [ { professorId:?0 },{academicPeriod:?2}, {\$or: [" +
            "{courseSchedule: { \$elemMatch: { dayOfWeek: ?1, 'startsAt.hour': { \$lt: ?3 }, 'endsAt.hour': { \$gt: ?3 } } } }," +
            "{courseSchedule: { \$elemMatch: { dayOfWeek: ?1, 'startsAt.hour': { \$lt: ?4 }, 'endsAt.hour': { \$gt: ?4 } } } }," +
            "{courseSchedule: { \$elemMatch: { dayOfWeek: ?1, 'startsAt.hour': ?3 , 'startsAt.minute': { \$lte: ?5 } } } }," +
            "{courseSchedule: { \$elemMatch: { dayOfWeek: ?1, 'endsAt.hour': ?3 , 'endsAt.minute': { \$gt: ?5 } } } }," +
            "{courseSchedule: { \$elemMatch: { dayOfWeek: ?1, 'startsAt.hour': ?4 , 'startsAt.minute': { \$lt: ?6 } } } }," +
            "{courseSchedule: { \$elemMatch: { dayOfWeek: ?1, 'endsAt.hour': ?4 , 'endsAt.minute': { \$gte: ?6 } } } }" +
            "] } ] }"
    )
    fun findProfessorCourseScheduleBetweenTwoDates(
        professorId: String,
        dayOfWeek: DayOfWeek,
        academicPeriod: String,
        startsAtHour: Int,
        endsAtHour: Int,
        startsAtMinute: Int,
        endsAtMinute: Int,
    ): Course?

    @Query("{academicSpaceId: ?0, group: ?1, dayTime:?2}")
    fun findCourseByAcademicSpaceGroupAndDayTime(academicSpaceId: String, group: String?, dayTime: DayTimeEnum?): Course?
}
