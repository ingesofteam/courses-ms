package edu.eam.ingesoft.courses.model.responses

data class QualificationResponse(
    val name: String,
    val weight: Double,
    val value: Double,
    var delivered: Boolean?
)
