package edu.eam.ingesoft.courses.model.entities

import org.springframework.format.annotation.DateTimeFormat
import java.util.Date

data class CourseStudentAttendance(
    val quantity: Int = 0,

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    val createdAt: Date? = Date(),

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    val notAttendanceAt: Time?,
)
