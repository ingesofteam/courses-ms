package edu.eam.ingesoft.courses.model.entities

import edu.eam.ingesoft.courses.model.enums.DayTimeEnum
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "courses")
data class Course(
    @Id
    val id: String? = null,

    var group: String? = "A",

    var professorId: String,

    var academicSpaceId: String,

    var dayTime: DayTimeEnum? = DayTimeEnum.DAY,

    var academicPeriod: String,

    var courseSchedule: MutableList<CourseSchedule>? = mutableListOf()
)
