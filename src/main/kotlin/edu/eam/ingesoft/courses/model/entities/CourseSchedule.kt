package edu.eam.ingesoft.courses.model.entities

import java.time.DayOfWeek

data class CourseSchedule(
    val dayOfWeek: DayOfWeek,

    val startsAt: Time,

    val endsAt: Time,
)
