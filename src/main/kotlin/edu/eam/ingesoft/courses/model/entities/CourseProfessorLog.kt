package edu.eam.ingesoft.courses.model.entities

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.format.annotation.DateTimeFormat
import java.util.Date

@Document(collection = "course_professor_logs")
data class CourseProfessorLog(
    @Id
    val id: String?,

    val description: String,

    val courseId: String,

    val hours: Int,

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    val createdAt: Date? = Date(),

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    val logDate: Time?,

    val checked: Boolean? = false,
)
