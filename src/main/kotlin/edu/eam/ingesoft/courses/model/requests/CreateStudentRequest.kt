package edu.eam.ingesoft.courses.model.requests

data class CreateStudentRequest(
    val studentId: String
)
