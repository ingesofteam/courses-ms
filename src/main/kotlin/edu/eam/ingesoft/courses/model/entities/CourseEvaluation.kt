package edu.eam.ingesoft.courses.model.entities

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "course_evaluations")
data class CourseEvaluation(
    @Id
    val id: String? = null,

    val courseId: String,

    val weight: Double,

    val name: String,
)
