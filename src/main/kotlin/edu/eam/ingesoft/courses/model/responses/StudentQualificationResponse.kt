package edu.eam.ingesoft.courses.model.responses

data class StudentQualificationResponse(

    val name: String,

    val weight: Double,

    val value: Double
)
