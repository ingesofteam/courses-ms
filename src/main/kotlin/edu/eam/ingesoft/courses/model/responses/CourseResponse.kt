package edu.eam.ingesoft.courses.model.responses

data class CourseResponse(
    val name: String,
    val id: String
)
