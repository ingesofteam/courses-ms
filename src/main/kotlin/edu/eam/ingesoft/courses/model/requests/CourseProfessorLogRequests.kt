package edu.eam.ingesoft.courses.model.requests

import edu.eam.ingesoft.courses.model.entities.Time
import java.time.DayOfWeek
import java.util.Date

data class CourseProfessorLogRequests(

    val description: String,
    val hours: Int,
    val createdAt: Date,
    val logDate: Time,
    val dayOfWeek: DayOfWeek
)
