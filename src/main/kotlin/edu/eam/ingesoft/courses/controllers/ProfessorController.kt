package edu.eam.ingesoft.courses.controllers

import edu.eam.ingesoft.courses.config.Groups
import edu.eam.ingesoft.courses.config.Permissions
import edu.eam.ingesoft.courses.config.Routes
import edu.eam.ingesoft.courses.model.requests.CourseProfessorLogRequests
import edu.eam.ingesoft.courses.security.Secured
import edu.eam.ingesoft.courses.services.CourseProfessorLogService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RequestMapping(Routes.PROFESSORS_PATH)
@RestController
class ProfessorController {

    @Autowired
    lateinit var courseProfessorLogService: CourseProfessorLogService

    @Secured(permissions = [Permissions.REGISTER_TEACHER_LOG], groups = [Groups.CLASS_TEACHER, Groups.SYSTEM_ADMINISTRATOR])
    @PostMapping(Routes.PATH_CREATE_PROFESSOR_LOG)
    fun createProfessorLog(
        @PathVariable courseId: String,
        @PathVariable id: String,
        @RequestBody requests: CourseProfessorLogRequests
    ) = courseProfessorLogService.create(
        id,
        requests.description,
        courseId,
        requests.hours,
        requests.createdAt,
        requests.logDate,
        requests.dayOfWeek
    )
}
