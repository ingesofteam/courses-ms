package edu.eam.ingesoft.courses.controllers

import edu.eam.ingesoft.courses.config.Routes
import edu.eam.ingesoft.courses.model.requests.CreateStudentRequest
import edu.eam.ingesoft.courses.services.CourseStudentService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(Routes.PATH_COURSES)
class StudentController {
    @Autowired
    lateinit var courseStudentService: CourseStudentService

    @PostMapping(Routes.CREATE_STUDENT_COURSE_PATH)
    fun createCourseStudent(@PathVariable("courseId") courseId: String, @RequestBody request: CreateStudentRequest) =
        courseStudentService.registerCourseStudent(courseId, request.studentId)
}
