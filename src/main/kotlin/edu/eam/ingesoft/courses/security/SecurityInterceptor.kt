package edu.eam.ingesoft.courses.security

import edu.eam.ingesoft.courses.security.authclient.SecurityService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.web.method.HandlerMethod
import org.springframework.web.servlet.HandlerInterceptor
import org.springframework.web.servlet.ModelAndView
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class SecurityInterceptor : HandlerInterceptor {

    @Autowired
    lateinit var securityService: SecurityService

    @Throws(Exception::class)
    fun getToken(request: HttpServletRequest): String {
        val bearer = request.getHeader("Authorization") ?: throw SecurityException("token required")
        if (!bearer.startsWith(prefix = "Bearer ")) {
            throw SecurityException("invalid token")
        }
        return bearer.substring(7)
    }

    @Throws(Exception::class)
    override fun preHandle(request: HttpServletRequest, response: HttpServletResponse, handler: Any): Boolean {

        if (handler is HandlerMethod) {

            val securityInfo: Secured = handler.method.getAnnotation(Secured::class.java) ?: return true

            val token = getToken(request)
            val securityPayload = securityService.validateToken(token)
            val userPermissions = securityPayload.permissions
            val userGroups = securityPayload.groups
            val handlerPermissions = securityInfo.permissions
            val handlerGroups = securityInfo.groups

            if (handlerPermissions.isEmpty() or handlerGroups.isEmpty()) {
                return true
            }

            if (handlerPermissions.intersect(userPermissions).isEmpty() or handlerGroups.intersect(userGroups).isEmpty()
            ) {
                throw SecurityException("this user has no access")
            }
        }
        return true
    }
    @Throws(Exception::class)
    override fun postHandle(
        request: HttpServletRequest,
        response: HttpServletResponse,
        handler: Any,
        modelAndView: ModelAndView?
    ) {
        println("Inside the Post Handle method")
    }

    @Throws(Exception::class)
    override fun afterCompletion(
        request: HttpServletRequest,
        response: HttpServletResponse,
        handler: Any,
        exception: Exception?
    ) {
        println("After completion of request and response")
    }
}
