package edu.eam.ingesoft.courses.security

import java.lang.RuntimeException

class SecurityException(message: String?) : RuntimeException(message)
