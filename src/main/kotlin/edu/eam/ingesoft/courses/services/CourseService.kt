package edu.eam.ingesoft.courses.services

import edu.eam.ingesoft.courses.exceptions.BusinessException
import edu.eam.ingesoft.courses.externalapi.model.requests.GetAcademicSpacesRequest
import edu.eam.ingesoft.courses.externalapi.services.AcademicMsService
import edu.eam.ingesoft.courses.externalapi.services.ProfessorMsService
import edu.eam.ingesoft.courses.externalapi.services.StudentsMsService
import edu.eam.ingesoft.courses.model.entities.Course
import edu.eam.ingesoft.courses.model.entities.CourseSchedule
import edu.eam.ingesoft.courses.model.responses.CourseRequest
import edu.eam.ingesoft.courses.model.responses.CourseResponse
import edu.eam.ingesoft.courses.model.responses.CourseStudentQualificationResponse
import edu.eam.ingesoft.courses.model.responses.CourseStudentQualificationsResponse
import edu.eam.ingesoft.courses.model.responses.QualificationResponse
import edu.eam.ingesoft.courses.model.responses.StudentQualificationResponse
import edu.eam.ingesoft.courses.repositories.CourseEvaluationRepository
import edu.eam.ingesoft.courses.repositories.CourseRepository
import edu.eam.ingesoft.courses.repositories.CourseStudentRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import javax.persistence.EntityNotFoundException

@Service
class CourseService {

    @Autowired
    lateinit var courseRepository: CourseRepository

    @Autowired
    lateinit var courseEvaluationRepository: CourseEvaluationRepository

    @Autowired
    lateinit var courseStudentRepository: CourseStudentRepository

    @Autowired
    lateinit var studentMsService: StudentsMsService

    @Autowired
    lateinit var academicMsService: AcademicMsService

    @Autowired
    lateinit var professorMsService: ProfessorMsService

    fun createCourse(course: CourseRequest) {

        val dayTime = course.dayTime
        val group = course.group
        val academicPeriodToFind = academicMsService.findAcademicPeriod()

        academicMsService.findAcademicSpace(course.academicSpaceId)
        professorMsService.findProfessor(course.professorId)

        val courseToFind = courseRepository.findCourseByAcademicSpaceGroupAndDayTime(course.academicSpaceId, group, dayTime)

        if (courseToFind != null) {

            throw BusinessException("Course is already registered.", HttpStatus.PRECONDITION_FAILED)
        }
        val courseToSave = Course(course.id, group, course.professorId, course.academicSpaceId, dayTime, academicPeriodToFind.id, null)
        courseRepository.save(courseToSave)
    }

    fun findCourseById(id: String): Course? =
        courseRepository.findById(id).orElseThrow { EntityNotFoundException("Course not found") }

    fun editCourse(id: String, course: Course): Course {
        val courseFound = courseRepository.findById(id).orElseThrow { EntityNotFoundException("Course not found") }

        courseFound.apply {
            group = course.group ?: group
            professorId = course.professorId
            dayTime = course.dayTime ?: dayTime
            academicSpaceId = course.academicSpaceId
            academicPeriod = course.academicPeriod
        }

        return courseRepository.save(courseFound)
    }

    fun createSchedule(courseId: String, schedule: CourseSchedule) {
        val course = courseRepository.findById(courseId)
            .orElseThrow { EntityNotFoundException("Course Not Found") }

        if (schedule.endsAt.hour < schedule.startsAt.hour ||
            (
                schedule.endsAt.hour == schedule.startsAt.hour &&
                    schedule.endsAt.minute <= schedule.startsAt.minute
                )
        )
            throw BusinessException(
                "Invalid Schedule's Duration",
                HttpStatus.PRECONDITION_FAILED
            )

        val coursesCross = courseRepository.findProfessorCourseScheduleBetweenTwoDates(
            course.professorId,
            schedule.dayOfWeek,
            course.academicPeriod,
            schedule.startsAt.hour,
            schedule.endsAt.hour,
            schedule.startsAt.minute,
            schedule.endsAt.minute
        )

        if (coursesCross != null)
            throw BusinessException("Schedule already programmed in that time period", HttpStatus.PRECONDITION_FAILED)

        course.courseSchedule?.add(schedule)

        courseRepository.save(course)
    }

    fun findQualificationsByStudent(studentCode: String): List<CourseStudentQualificationResponse> {

        studentMsService.findStudentByCode(studentCode)

        val studentCourses = courseStudentRepository.findByStudentId(studentCode)

        if (studentCourses.isNullOrEmpty()) throw BusinessException(
            "Student has not been assigned a course",
            HttpStatus.NOT_FOUND
        )

        val courses = courseRepository.findAllById(studentCourses.map { it.courseId })

        val academicSpaces =
            academicMsService.findAcademicSpacesByIds(GetAcademicSpacesRequest(courses.map { it.academicSpaceId.toLong() }))

        return studentCourses.map { it ->

            var finalNote = 0.0

            val evaluations =
                courseEvaluationRepository.findAllById(it.courseStudentQualification?.map { it.courseEvaluationId }!!)

            val qualifications = it.courseStudentQualification.map { qualification ->

                val evaluation = evaluations.find { evaluation -> evaluation.id == qualification.courseEvaluationId }

                finalNote += qualification.value!! * evaluation!!.weight

                QualificationResponse(evaluation.name, evaluation.weight, qualification.value!!, qualification.delivered)
            }

            val academicSpace =
                academicSpaces.find { academicSpaces -> academicSpaces.id.toString() == (courses.find { courses -> courses.id == it.courseId })!!.academicSpaceId }

            val course = CourseResponse(academicSpace!!.name, academicSpace.id.toString())

            CourseStudentQualificationResponse(course, qualifications, finalNote)
        }
    }

    fun findQualificationsByCourse(courseId: String, pageable: Pageable): Page<CourseStudentQualificationsResponse> {

        courseRepository.findById(courseId).orElseThrow { EntityNotFoundException("Course not found") }

        val courseStudents = courseStudentRepository.findByCourseId(courseId, pageable)

        return courseStudents.map { it ->

            var finalNote = 0.0

            val evaluations = courseEvaluationRepository.findAllById(it.courseStudentQualification?.map { it.courseEvaluationId }!!)

            val qualifications = it.courseStudentQualification.map { qualification ->
                val evaluation = evaluations.find { evaluation -> evaluation.id == qualification.courseEvaluationId }
                finalNote += qualification.value!! * evaluation!!.weight
                StudentQualificationResponse(evaluation.name, evaluation.weight, qualification.value!!)
            }

            val studentId = it.studentId

            CourseStudentQualificationsResponse(studentId, qualifications, finalNote)
        }
    }
}
