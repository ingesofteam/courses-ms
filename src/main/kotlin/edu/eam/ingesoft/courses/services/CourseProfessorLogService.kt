package edu.eam.ingesoft.courses.services

import edu.eam.ingesoft.courses.exceptions.BusinessException
import edu.eam.ingesoft.courses.model.entities.CourseProfessorLog
import edu.eam.ingesoft.courses.model.entities.Time
import edu.eam.ingesoft.courses.repositories.CourseProfessorLogRepository
import edu.eam.ingesoft.courses.repositories.CourseRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import java.time.DayOfWeek
import java.util.Date
import javax.persistence.EntityNotFoundException

@Service
class CourseProfessorLogService() {
    @Autowired
    lateinit var courseProfessorLogRepository: CourseProfessorLogRepository

    @Autowired
    lateinit var courseRepository: CourseRepository

    fun editCourseProfessorLog(
        id: String,
        description: String,
        courseId: String,
        hours: Int,
        createdAt: Date,
        logDate: Time,
        dayOfWeek: DayOfWeek
    ) {
        val courseToFind = courseRepository.findById(courseId)

        if (courseToFind.isEmpty) throw EntityNotFoundException("Course is not found")

        val professorLogToFind = courseProfessorLogRepository.findByIdAndCourseId(id)
            ?: throw EntityNotFoundException("Professor Log not found")

        if (professorLogToFind.checked == true) throw BusinessException(
            "Checked is true",
            HttpStatus.PRECONDITION_FAILED
        )

        courseToFind.get().courseSchedule?.find {
            (it.startsAt.hour <= logDate.hour && it.endsAt.hour >= logDate.hour) && it.dayOfWeek == dayOfWeek
        }
            ?: throw BusinessException("The course date does not correspond", HttpStatus.PRECONDITION_FAILED)

        courseProfessorLogRepository.save(
            CourseProfessorLog(
                id,
                description,
                courseId,
                hours,
                createdAt,
                logDate,
                false
            )
        )
    }

    fun delete(id: String, courseId: String) {
        val courseToFind = courseRepository.findById(courseId)

        if (courseToFind.isEmpty) throw EntityNotFoundException("Course is not found")

        val professorLogToFind = courseProfessorLogRepository.findByIdAndCourseId(id)
            ?: throw EntityNotFoundException("Professor Log not found")

        if (professorLogToFind.checked == true) throw BusinessException(
            "Checked is true",
            HttpStatus.PRECONDITION_FAILED
        )

        courseProfessorLogRepository.delete(professorLogToFind)
    }

    fun create(
        id: String,
        description: String,
        courseId: String,
        hours: Int,
        createdAt: Date,
        logDate: Time,
        dayOfWeek: DayOfWeek
    ) {

        val course = courseRepository.findById(courseId)

        if (course.isEmpty) throw EntityNotFoundException("This course was not found")

        val courseProfessorLog = courseProfessorLogRepository.findById(id)
        if (courseProfessorLog.isPresent) throw BusinessException("ProfessorLog already exist")

        val courseScheduleToFind = course.get().courseSchedule?.find {
            (it.startsAt.hour <= logDate.hour && it.endsAt.hour >= logDate.hour) && (it.startsAt.hour <= hours && it.endsAt.hour >= hours) && it.dayOfWeek == dayOfWeek
        }
            ?: throw BusinessException("This log date does not match with the selected course")

        if (courseScheduleToFind.startsAt.hour == hours || courseScheduleToFind.endsAt.hour == hours)
            throw BusinessException("ProfessorLog is in this already")

        courseProfessorLogRepository.save(
            CourseProfessorLog(
                id,
                description,
                courseId,
                hours,
                createdAt,
                logDate,
                true
            )
        )
    }
}
