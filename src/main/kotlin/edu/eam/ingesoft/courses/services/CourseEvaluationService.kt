package edu.eam.ingesoft.courses.services

import edu.eam.ingesoft.courses.exceptions.BusinessException
import edu.eam.ingesoft.courses.model.entities.CourseEvaluation
import edu.eam.ingesoft.courses.repositories.CourseEvaluationRepository
import edu.eam.ingesoft.courses.repositories.CourseRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import javax.persistence.EntityNotFoundException

@Service
class CourseEvaluationService {

    @Autowired
    lateinit var courseRepository: CourseRepository

    @Autowired
    lateinit var courseEvaluationRepository: CourseEvaluationRepository

    fun createCourseEvaluation(courseId: String, name: String, weight: Double) {

        if (!courseRepository.existsById(courseId)) throw EntityNotFoundException("Course not found")

        if (courseEvaluationRepository.existsByName(name)) throw BusinessException(
            "Another evaluation is already using name of the current evaluation",
            HttpStatus.PRECONDITION_FAILED
        )

        if (weight.isNaN() || weight <= 0.0 || weight > 1.0) throw BusinessException(
            "The weight entered must be greater than 0% and less than or equal to 100%",
            HttpStatus.PRECONDITION_FAILED
        )

        val evaluations = courseEvaluationRepository.findByCourseId(courseId)

        var actualWeight = 0.0

        if (!evaluations.isNullOrEmpty()) {
            actualWeight = evaluations.map { it.weight }.sum()
        }

        if (actualWeight + weight > 1.0) throw BusinessException(
            "The weight entered is exceeds the 100% of the course evaluation",
            HttpStatus.PRECONDITION_FAILED
        )

        courseEvaluationRepository.save(CourseEvaluation(courseId = courseId, weight = weight, name = name))
    }
}
