package edu.eam.ingesoft.courses.services

import edu.eam.ingesoft.courses.exceptions.BusinessException
import edu.eam.ingesoft.courses.externalapi.services.StudentsMsService
import edu.eam.ingesoft.courses.model.entities.CourseStudent
import edu.eam.ingesoft.courses.model.entities.CourseStudentAttendance
import edu.eam.ingesoft.courses.model.entities.Time
import edu.eam.ingesoft.courses.repositories.CourseRepository
import edu.eam.ingesoft.courses.repositories.CourseStudentRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import java.time.DayOfWeek
import java.util.Date
import javax.persistence.EntityNotFoundException

@Service
class CourseStudentService {

    @Autowired
    lateinit var courseStudentRepository: CourseStudentRepository

    @Autowired
    lateinit var courseRepository: CourseRepository

    @Autowired
    lateinit var studentsMsService: StudentsMsService

    fun createFailure(studentId: String, courseId: String, notAttendanceAt: Time, dayOfWeek: DayOfWeek, createdAt: Date, quantity: Int) {

        val courseToFind = courseRepository.findById(courseId)

        courseToFind.get().courseSchedule?.find {
            (it.startsAt.hour <= notAttendanceAt.hour && it.endsAt.hour >= notAttendanceAt.hour) && it.dayOfWeek == dayOfWeek
        }
            ?: throw BusinessException("The course schedule does not match with this date time.", HttpStatus.PRECONDITION_FAILED)

        val courseStudentToFind = courseStudentRepository.findByStudentIdAndCourseId(studentId, courseId)
            ?: throw EntityNotFoundException("Course's student does not exist.")

        val failureToFind = courseStudentRepository.findCourseStudentByIdAndDate(studentId, courseId, createdAt, notAttendanceAt.hour, notAttendanceAt.minute)
        if (failureToFind != null) throw BusinessException("This failure has already been registered.", HttpStatus.PRECONDITION_FAILED)

        courseStudentToFind.courseStudentAttendance?.add(CourseStudentAttendance(notAttendanceAt = notAttendanceAt, createdAt = createdAt, quantity = quantity))
        courseStudentRepository.save(courseStudentToFind)
    }

    fun createGrade(studentId: String, evaluationId: String, courseId: String, newGrade: Double) {

        val courseStudentToFind = courseStudentRepository.findByStudentIdAndCourseId(studentId, courseId)
            ?: throw BusinessException("Course student does not exist.", HttpStatus.NOT_FOUND)

        courseStudentRepository.findEvaluationByIdAndCourse(studentId, courseId, evaluationId)
            ?: throw BusinessException("Evaluation has not been registered yet.", HttpStatus.PRECONDITION_FAILED)

        if (newGrade > 5.0 || newGrade < 0.0) throw BusinessException("Grades must be between 0 and 5.", HttpStatus.PRECONDITION_FAILED)

        val courseEvaluationToFind = courseStudentToFind.courseStudentQualification?.find { it.courseEvaluationId == evaluationId }
            ?: throw BusinessException("Evaluation was not founded.", HttpStatus.PRECONDITION_FAILED)

        if (courseEvaluationToFind.delivered == true) throw BusinessException("The evaluation has been assigned already.", HttpStatus.PRECONDITION_FAILED)

        courseEvaluationToFind.value = newGrade
        courseEvaluationToFind.delivered = true

        courseStudentRepository.save(courseStudentToFind)
    }

    fun registerCourseStudent(courseId: String, studentId: String) {

        val courseToFind = courseRepository.findById(courseId)

        if (courseToFind.isEmpty) throw BusinessException(
            "Course Not Found",
            HttpStatus.NOT_FOUND
        )

        val findStudentInCourse = courseStudentRepository.findStudentInCourse(courseId, studentId)

        if (findStudentInCourse != null) throw BusinessException(
            "Student Is Already In The Course",
            HttpStatus.PRECONDITION_FAILED
        )
        studentsMsService.findStudentByCode(studentId)

        courseStudentRepository.save(CourseStudent(null, studentId, courseId, null, null))
    }

    fun deliverGreadStudent(courseId: String, studentId: String, evaluationId: String) {

        if (!courseRepository.existsById(courseId)) throw BusinessException(
            "The course does not exist",
            HttpStatus.NOT_FOUND
        )

        val courseStudents: CourseStudent? =
            courseStudentRepository.findCourseStudentQualification(studentId, courseId, evaluationId)

        val eval = courseStudents?.courseStudentQualification?.find { it.courseEvaluationId == evaluationId }
            ?: throw BusinessException(
                "The evaluation does not exist on this curse student",
                HttpStatus.NOT_FOUND
            )

        if (eval.delivered == true) {
            throw BusinessException("This grade is alredy delivered", HttpStatus.PRECONDITION_FAILED)
        } else {
            eval.delivered = true
        }

        courseStudentRepository.save(courseStudents)
    }

    fun editCalification(
        courseId: String,
        studentId: String,
        evaluationId: String,
        value: Double,
        createdAt: Date,
        delivered: Boolean = false
    ) {

        val course = courseRepository.findById(courseId)

        if (course.isEmpty) throw EntityNotFoundException("Course not found")

        val courseStudent = courseStudentRepository.findByStudentIdAndCourseId(studentId, courseId)
            ?: throw EntityNotFoundException("Student course not found")

        val courseEvaluationToFind = courseStudent.courseStudentQualification?.find { it.courseEvaluationId == evaluationId }
            ?: throw BusinessException("Evaluation was not founded.", HttpStatus.PRECONDITION_FAILED)

        if (courseEvaluationToFind.delivered == true) throw BusinessException("The evaluation has been assigned already.", HttpStatus.PRECONDITION_FAILED)

        courseStudent.courseStudentQualification.map {
            if (it.courseEvaluationId == evaluationId) {
                it.courseEvaluationId = evaluationId
                it.value = value
            }
        }

        courseStudentRepository.save(courseStudent)
    }
}
