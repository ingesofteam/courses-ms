package edu.eam.ingesoft.courses.externalapi.services

import edu.eam.ingesoft.courses.externalapi.clients.ProfessorMsClient
import edu.eam.ingesoft.courses.externalapi.model.responses.ProfessorResponse
import feign.FeignException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.lang.Exception
import javax.persistence.EntityNotFoundException

@Component
class ProfessorMsService {

    @Autowired
    lateinit var professorMsClient: ProfessorMsClient

    fun findProfessor(code: String): ProfessorResponse {
        try {
            return professorMsClient.getProfessor(code)
        } catch (exc: FeignException.NotFound) {
            throw EntityNotFoundException("Professor not found.")
        } catch (exc: FeignException) {
            throw Exception("Unexpected Exception")
        }
    }
}
