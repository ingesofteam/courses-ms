package edu.eam.ingesoft.courses.externalapi.model.responses

data class AcademicPeriodResponse(
    val id: String
)
