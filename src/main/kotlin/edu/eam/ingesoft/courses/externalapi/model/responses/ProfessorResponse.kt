package edu.eam.ingesoft.courses.externalapi.model.responses

data class ProfessorResponse(
    val code: String,

    val identificationNumber: String
)
