package edu.eam.ingesoft.courses.externalapi.clients

import edu.eam.ingesoft.courses.externalapi.model.responses.ProfessorResponse
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod

@FeignClient(name = "ProfessorMsClient", url = "\${externalapi.professors-ms}/api/professors-ms")
interface ProfessorMsClient {
    @RequestMapping(method = [RequestMethod.GET], path = ["/professors/{code}"])
    fun getProfessor(@PathVariable code: String): ProfessorResponse
}
