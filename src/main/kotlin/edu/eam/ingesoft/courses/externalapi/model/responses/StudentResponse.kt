package edu.eam.ingesoft.courses.externalapi.model.responses

data class StudentResponse(
    val code: String
)
