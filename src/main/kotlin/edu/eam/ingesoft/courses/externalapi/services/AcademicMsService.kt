package edu.eam.ingesoft.courses.externalapi.services

import edu.eam.ingesoft.courses.externalapi.clients.AcademicMsClient
import edu.eam.ingesoft.courses.externalapi.model.requests.GetAcademicSpacesRequest
import edu.eam.ingesoft.courses.externalapi.model.responses.AcademicPeriodResponse
import edu.eam.ingesoft.courses.externalapi.model.responses.AcademicSpaceResponse
import feign.FeignException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.lang.Exception
import javax.persistence.EntityNotFoundException

@Component
class AcademicMsService {

    @Autowired
    lateinit var academicMsClient: AcademicMsClient

    fun findAcademicSpacesByIds(getAcademicSpacesRequest: GetAcademicSpacesRequest): List<AcademicSpaceResponse> {
        try {
            return academicMsClient.getAcademicSpacesByIds(getAcademicSpacesRequest)
        } catch (exc: FeignException) {
            throw Exception("Unexpected Exception")
        }
    }

    fun findAcademicSpace(idAcademicSpace: String): AcademicSpaceResponse {
        try {
            return academicMsClient.getAcademicSpace(idAcademicSpace)
        } catch (exc: FeignException.NotFound) {
            throw EntityNotFoundException("Academic space doesn't exist.")
        } catch (exc: FeignException) {
            throw Exception("Unexpected Exception")
        }
    }

    fun findAcademicPeriod(): AcademicPeriodResponse {
        try {
            return academicMsClient.getAcademicPeriod()
        } catch (exc: FeignException) {
            throw Exception("Unexpected Exception")
        }
    }
}
